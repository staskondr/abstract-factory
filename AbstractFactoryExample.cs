public interface IButton
{
    void Draw();
}

public class WinButton : IButton
{
    public void Draw() {
        //do some action for windows button draw 
    }
}

public class MacButton : IButton
{
    public void Draw() {
        //do some action for mac button draw
    }
}

public interface ICheckBox
{
    void Draw();
}

public class WinCheckBox: ICheckBox
{
    public void Draw() {
        //do some action for windows checkbox draw 
    }
}

public class MacCheckBox : ICheckBox
{
    public void Draw() {
        //do some action for mac checkbox draw 
    }
}

public interface IGuiFactory
{
    IButton CreateButton();

    ICheckBox CreateCheckBox();
}

public class WinGuiFactory: IGuiFactory
{
    public IButton CreateButton() {
        return new WinButton();
    }

    public ICheckBox CreateCheckBox() {
        return new WinCheckBox();
    }
}


public class MacGuiFactory : IGuiFactory
{
    public IButton CreateButton() {
        return new MacButton();
    }

    public ICheckBox CreateCheckBox() {
        return new MacCheckBox();
    }
}