﻿using System;

namespace PatternsLesson1
{
    public class Program
    {
        private const string MacOs = "Mac";
        private const string WinOs = "Win";

        public static void Main(string[] args) {
            if (args.Length < 1) {
                throw new ArgumentException($"Incorrect argument, should be \"{MacOs}\" or \"{WinOs}\"");
            }

            IGuiFactory factory;
            var os = args[0];
            switch (os) {
                case MacOs:
                    factory = new MacGuiFactory();
                    break;
                case WinOs:
                    factory = new WinGuiFactory();
                    break;
                default:
                    throw new ArgumentException($"Incorrect argument, should be \"{MacOs}\" or \"{WinOs}\"");
            }
            var application = new Application(factory);
            Console.WriteLine(application.Button.GetType());
            Console.WriteLine(application.CheckBox.GetType());
            Console.Read();
        }
    }
}
