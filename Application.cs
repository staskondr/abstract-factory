﻿namespace PatternsLesson1
{
    public class Application
    {
        private IGuiFactory _factory;
        public IButton Button { get; protected set; }

        public ICheckBox CheckBox { get; protected set; }

        public Application(IGuiFactory factory)
        {
            _factory = factory;
            Button = _factory.CreateButton();
            CheckBox = _factory.CreateCheckBox();
        }
    }
}